from flask import Flask, request, jsonify, send_file
from dotenv import dotenv_values
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from io import BytesIO
from PIL import Image
import io
import qrcode
import requests
import speedtest
import pytesseract
# import numpy as np
# import cv2

config = dotenv_values(".env")

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = "postgresql://root:tOjtWsxFPce8UJSnhMF8DjS0@postgres:5432/utilities"
db = SQLAlchemy(app)
migrate = Migrate(app, db)

# class Users(db.Model):
#     id = db.Column(db.Integer, primary_key=True)
#     username = db.Column(db.String(64), index=True, unique=True)
#     email = db.Column(db.String(120), index=True, unique=True)
    
#     def __repr__(self):
#         return '<User {}>'.format(self.username)
    
#     def to_dict(self):
#         return {
#             'id': self.id,
#             'username': self.username,
#             'email': self.email
#         }

# @app.route('/get_users', methods=['GET'])
# def get_users():
#     users = Users.query.all()
    
#     return jsonify([user.to_dict() for user in users])

# @app.route('/get_user/<int:id>', methods=['GET'])
# def get_user(id):
#     user = Users.query.get(id)
    
#     return jsonify(user.to_dict())
    
# @app.route('/create_user', methods=['POST'])
# def create_user():
#     user = Users(username=request.json['username'], email=request.json['email'])
#     db.session.add(user)
#     db.session.commit()
    
#     return 'User added'

# @app.route('/update_user/<int:id>', methods=['PUT'])
# def update_user(id):
#     user = Users.query.get(id)
#     user.username = request.json['username']
#     user.email = request.json['email']
#     db.session.commit()
    
#     return "User updated successfully"

# @app.route('/delete_user/<int:id>', methods=['DELETE'])
# def delete_user(id):
#     user = Users.query.get(id)
#     db.session.delete(user)
#     db.session.commit()
    
#     return "User deleted successfully"

@app.route('/generate_qrcode', methods=['POST'])
def generate_qrcode():
    qr = qrcode.QRCode(version=1, box_size=10, border=5)
    qr.add_data(request.json['text'])
    qr.make(fit=True)
    img = qr.make_image(fill_color="black", back_color="white")
    
    buffer = BytesIO()
    img.save(buffer, 'PNG')
    buffer.seek(0)
    
    return send_file(buffer, mimetype='image/png')

# Todo: Make a client request instead
@app.route("/currencies_conversion", methods=["POST"])
def currencies_conversion():
    response = requests.get(f"https://api.apilayer.com/exchangerates_data/convert?amount=1000&apikey=BT57v1BmHVf20Ssyq5cFcceU2PXGbeWd&from=USD&to=THB")
    
    if response.status_code == 200:
        return response.content
    else:
        return 'Request failed with status code: ', response.status_code
    
@app.route('/check_internet_speed', methods=['GET'])
def check_internet_speed():
    st = speedtest.Speedtest()
    download_speed = st.download() / 1_000_000  # convert to megabits per second
    upload_speed = st.upload() / 1_000_000  # convert to megabits per second

    return jsonify({
        'download_speed': f'{download_speed:.2f} Mbps',
        'upload_speed': f'{upload_speed:.2f} Mbps'
    })

@app.route('/image_to_text', methods=['POST'])
def image_to_text():
    file = request.files['image']

    img = Image.open(io.BytesIO(file.read()))

    text = pytesseract.image_to_string(img)

    return text


# @app.route('/remove_censor', methods=['POST'])
# def remove_censor():
#     # Load the image from the request
#     img = cv2.imdecode(np.frombuffer(request.data, np.uint8), cv2.IMREAD_UNCHANGED)
    
#     # Convert the image to grayscale
#     gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    
#     # Apply a Gaussian blur to reduce noise
#     blurred = cv2.GaussianBlur(gray, (7, 7), 0)
    
#     # Threshold the blurred image to create a binary image
#     _, thresholded = cv2.threshold(blurred, 200, 255, cv2.THRESH_BINARY)
    
#     # Invert the binary image
#     thresholded = cv2.bitwise_not(thresholded)
    
#     # Find contours in the binary image
#     contours, _ = cv2.findContours(thresholded, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    
#     # Iterate through the contours and draw rectangles over the censor regions
#     for contour in contours:
#         x, y, w, h = cv2.boundingRect(contour)
#         img[y:y+h, x:x+w] = 255
        
#     # Encode the output image as a binary string
#     retval, buffer = cv2.imencode('.png', img)
    
#     # Return the output image as a binary string
#     return buffer.tobytes(), 200, {'Content-Type': 'image/png'}